import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Enemy {

	int x;
	int y;
	int v;
	int width = 353;
	int height = 133;

	Image img_boom = new ImageIcon("src/main/resources/img/boom.png").getImage();
	List<Image> listImage = new ArrayList<>();
	Random random = new Random();

	public Image generateEnymyColor(){
		listImage.add(new ImageIcon("src/main/resources/img/enemy_blue.png").getImage());
		listImage.add(new ImageIcon("src/main/resources/img/enemy_gray.png").getImage());
		listImage.add(new ImageIcon("src/main/resources/img/enemy_green.png").getImage());
		listImage.add(new ImageIcon("src/main/resources/img/enemy_pink.png").getImage());
		listImage.add(new ImageIcon("src/main/resources/img/enemy_red.png").getImage());
		listImage.add(new ImageIcon("src/main/resources/img/enemy_yellow.png").getImage());


		Image imgs = listImage.get(random.nextInt(listImage.size()));
		return imgs;
	}

	Image img = generateEnymyColor();
	Road road;

	public  Rectangle getRect(){
		return new Rectangle(x, y, width, height);
	}

	public Enemy(int x, int y, int v, Road road){
		this.x = x;
		this.y = y;
		this.v = v;
		this.road = road;
	}

	public void move(){
		x =  x - road.p.v + v;
	}
}
