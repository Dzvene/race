import javax.swing.*;

public class Main {

	public static void main(String[] args) {
		JFrame f = new JFrame("Java Rice");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setSize(1000, 600);
		f.getContentPane().add(new Road());
		f.setVisible(true);
		f.setResizable(false);
	}
}
