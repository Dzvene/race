import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;

public class Player {

	public static final int MAX_V = 50;
	public static final int MAX_TOP = 10;
	public static final int MAX_BOTTOM = 450;

	Image imgCentre = new ImageIcon("src/main/resources/img/player.png").getImage();
	Image imgUp = new ImageIcon("src/main/resources/img/player_up.png").getImage();
	Image imgDdovn = new ImageIcon("src/main/resources/img/player_down.png").getImage();
	Image imgAv = new ImageIcon("src/main/resources/img/avaria.png").getImage();
	Image img = imgCentre;

	int v = 0;
	int dv = 0;
	int s = 0;
	int x = 100;
	int y = 30;
	int dy = 0;
	int layer1 = 0;
	int layer2 = 1000;

	public  Rectangle getRect(){
		return new Rectangle(x, y, 315, 105);
	}

	public void move(){
		s += v;
		v += dv;

		if(v <= 0){
			v = 0;
		}

		if(v >= MAX_V){
			v = MAX_V;
		}

		y -= dy;

		if(y <= MAX_TOP){
			y = MAX_TOP;
		}

		if(y >= MAX_BOTTOM){
			y = MAX_BOTTOM;
		}

		if(layer2 - v <- 0){
			layer1 = 0;
			layer2 = 1000;
		}else{
			layer1 -= v;
			layer2 -= v;
		}
	}

	public void keyPressed(KeyEvent e) {
		int key = e.getExtendedKeyCode();

		if(key == KeyEvent.VK_RIGHT){
			dv = 1;
		}
		if(key == KeyEvent.VK_LEFT){
			dv = -1;
		}

		if(key == KeyEvent.VK_UP){
			dy = 15;
			img = imgUp;
		}
		if(key == KeyEvent.VK_DOWN){
			dy = -15;
			img = imgDdovn;
		}

	}

	public void keyReleased(KeyEvent e) {
		int key = e.getExtendedKeyCode();

		if(key == KeyEvent.VK_LEFT || key == KeyEvent.VK_LEFT) {
			dv = 0;
			img = imgCentre;
		}

		if(key == KeyEvent.VK_UP || key == KeyEvent.VK_DOWN) {
			dy = 0;
			img = imgCentre;
		}
	}
}
