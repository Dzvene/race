import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class Road extends JPanel implements ActionListener, Runnable{

	Timer mainTimer = new Timer(20, this);
	Image img = new ImageIcon("src/main/resources/img/road.jpg").getImage();
	Player p = new Player();
	Thread enemyFactory = new Thread(this);
	Thread bonusFactory = new Thread(this);
	Thread audioFactory = new Thread(new AudioThread());
	List<Enemy> enemyList  = new ArrayList<>();

  	int countBoom = 0;
	int timeGame = 3500;

	public Road(){
		mainTimer.start();
		enemyFactory.start();
		audioFactory.start();
		bonusFactory.start();
		addKeyListener(new MyKeyAdapter());
		setFocusable(true);
	}

	private class MyKeyAdapter extends KeyAdapter{
		public void  keyPressed(KeyEvent e){
			p.keyPressed(e);
		}
		public void  keyReleased(KeyEvent e){
			p.keyReleased(e);
		}
	}

	public void paint(Graphics g){
		g = (Graphics2D) g;
		g.drawImage(img, p.layer1, 0, null);
		g.drawImage(img, p.layer2, 0, null);
		g.drawImage(p.img, p.x, p.y, null);

		double speed = (200 / Player.MAX_V) * p.v;

		Iterator<Enemy> i = enemyList.iterator();

		g.setColor(Color.WHITE);
		Font font = new Font("Arial", Font.BOLD, 20);
		g.setFont(font);

		((Graphics2D) g).drawString("Скорость: " + speed + " км.ч", 50, 30);
		((Graphics2D) g).drawString("Дистанция: " + p.s + " км", 300, 30);
		((Graphics2D) g).drawString("Аварии: " + countBoom, 600, 30);
		((Graphics2D) g).drawString("Время: " + timeGame + " мс", 800, 30);

		while (i.hasNext()){
			Enemy e = i.next();
			if(e.x >= 2400 || e.x <= -2400){
				i.remove();
			}else{
				e.move(); //TODO переделать
				g.drawImage(e.img, e.x, e.y, null);
			}
		}

		timeGame--;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		p.move();
		repaint();
		testCollisionWithEnemy();
		testWin();
	}

	private void testWin() {

		if(p.s > 150000 && countBoom < 20){
			JOptionPane.showConfirmDialog(null, "Ты выииграл");
			System.exit(0);
		}

		if(p.s > 150000 && countBoom > 20){
			JOptionPane.showConfirmDialog(null, "Ты проиграл");
			System.exit(0);
		}

		if(timeGame == 0){
			JOptionPane.showConfirmDialog(null, "Ты проиграл");
			System.exit(0);
		}
	}

	private void testCollisionWithEnemy() {

		Iterator<Enemy> i = enemyList.iterator();

		while(i.hasNext()){
			Enemy enemy = i.next();
			if(p.getRect().intersects(enemy.getRect())){
				enemy.img = enemy.img_boom;
				enemy.v = 0;
				p.v = 10;
//				p.img = p.imgAv;
				enemy.width = 0;
				enemy.getRect();
				countBoom += 1;
			}

		}
	}

	@Override
	public void run() {

		while (true){
			Random random = new Random();
			try {
				Thread.sleep(random.nextInt(6000));
				enemyList.add(new Enemy(1100, random.nextInt(450), random.nextInt(70), this));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}


	}

}
